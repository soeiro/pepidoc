// functions --------------------------------------------------------------

// pandoc extensions/options could be used instead of some js functions:
// (but using js allow to swith to another Markdown parser more easely)
// - yaml_metadata_block extension + <title>$title$<title> instead of appendText()
// - [gfm_]auto_identifiers extension instead of makeId(); then resetHash() would not be necessary
// - --toc option intead of populateList()

// append the text of an element to another one
// setElm: an element whose text is to be set
// getElm: an element whose text is to be get
const appendText = (setElm, getElm) => {
  const text = ` | ${getElm.textContent}`;
  setElm.textContent += text;
};

// add an identifier to an element based on its text
// elm: an element
const makeId = elm => {
  const id = elm.textContent
    .toLowerCase()
    .normalize("NFD")
    .replace(/[^a-z0-9 ]/g, "")
    .trim()
    .replace(/ +/g, "-");
  elm.id = id;
};

// add an anchor to an element referencing itself
// elm: an element with an identifier
const makeAnchor = elm => {
  const a = document.createElement("a");
  a.href = `#${elm.id}`;
  a.innerHTML = elm.innerHTML;
  elm.innerHTML = a.outerHTML;
};

// add an item referencing an element into a list and set the item class according to the element tag name
// elm: an element to reference as an item
// list: a list to populate
const populateList = (elm, list) => {
  const li = document.createElement("li");
  li.innerHTML = elm.innerHTML;
  li.classList.add(elm.tagName);
  list.appendChild(li);
};

// reset URL hash if there is one
const resetHash = () => {
  if (window.location.hash) {
    const hash = window.location.hash;
    window.location.hash = "";
    window.location.hash = hash;
  }
};

// add the active class to an element when a corresponding element is observed
// (callback for IntersectionObserver())
// entries: an object of IntersectionObserverEntry objects for each observed target which reported a change in its intersection status
// obsElms: a NodeList of observed elements
// corElms: a NodeList of corresponding elements to add the active class to
const addActive = (entries, obsElms, corElms) => {
  entries.forEach(entry => {
    if (entry.isIntersecting) {
      const index = Array.from(obsElms).indexOf(entry.target);
      corElms.forEach(corElm => corElm.classList.remove("active"));
      corElms[index].classList.add("active");
    }
  });
};

// init -------------------------------------------------------------------

const title = document.querySelector("title");
const h1 = document.querySelector("h1");
appendText(title, h1);

const headings = document.querySelectorAll("h2, h3");
const toc = document.querySelector("nav ul");
headings.forEach(heading => {
  makeId(heading);
  makeAnchor(heading);
  populateList(heading, toc);
});

// jump to heading once identifiers are set
resetHash();

// observe intersections in the top 20% of the viewport
// if the first h2 is outside on load (e.g. due to a long h1 text), its toc anchor will not be marked as active
// if the nth h2 is inside on load, (e.g. due to a short first h2 section), its toc anchor will not be marked as active
// if the last h2 or h3 is outside on scroll, (e.g. due to a short last section), its toc anchor will not be marked as active
const anchors = toc.querySelectorAll("li a");
const observer = new IntersectionObserver(
  target => addActive(target, headings, anchors),
  { rootMargin: "0% 0% -80% 0%" }
);
headings.forEach(heading => observer.observe(heading));
