# Utilisation de Oracle et R sur le portail de la Cnam

## Introduction

Cette fiche a pour objectif de proposer un guide d’utilisation des logiciels disponibles sur le portail de la Cnam pour exploiter le SNDS. Elle se focalise en particulier sur Oracle et R. SAS est néanmoins brièvement présenté en raison de son importance historique. Une connaissance des bases de SQL et R est attendue.

## Oracle et SQL

La plupart des données du SNDS sont stockées dans une base de données Oracle. SQL est le langage permettant d’utiliser les bases de données. Il est nécessaire d’utiliser [SAS](#connexion-a-oracle-avec-sas) ou [R](#connexion-a-oracle-avec-dbi-et-odbc) pour accéder à Oracle. Il n’y a pas d’interface directe telle que SQL Developer.

Chaque utilisateur peut accéder à un certain périmètre de données selon le type d’accès et le profil de connexion. Le profil 108 permet d’accéder aux données exhaustives.

<div class="note">

Un système de synonymes est maintenu par les administrateurs du SNDS afin de masquer cette complexité pour les utilisateurs (voir la sortie de `SELECT * FROM user_synonyms;` selon le profil de connexion). Il est donc possible de requêter dans la plupart des tables et des vues sans préciser le schéma dans lequel elles se trouvent. La cartographie est une exception notable : il est nécessaire de préciser le schéma en plus de la vue (par exemple `MEPSGP_108` pour un utilisateur connecté avec un profil 108).

</div>

### SQL ANSI

SQL est une norme de l’ANSI qui est révisée régulièrement pour y inclure de nouvelles fonctionnalités. Malgré l’existence de cette norme, aucun éditeur n’y adhère complètement, [Oracle y compris](https://docs.oracle.com/en/database/oracle/oracle-database/19/sqlrf/Oracle-and-Standard-SQL.html). Cela signifie qu’un script SQL nécessitera souvent des modifications pour être porté sur une base de données d’un autre éditeur. Pour rendre le code moins dépendant d’un éditeur et faciliter son portage, on peut favoriser le SQL ANSI quand il y a le choix entre une méthode de la norme ANSI et une méthode propre à un éditeur.

<div class="note">

Cela pourrait notamment simplifier le portage des scripts développés pour Oracle sur le portail de la Cnam vers la plateforme du *Health Data Hub* qui propose actuellement PostgreSQL et MySQL.

</div>

### « Statistiques » des tables

Afin qu’Oracle sélectionne le meilleur plan d’exécution des instructions, il est nécessaire de collecter les « statistiques » des tables importée dans Oracle par les utilisateurs (par exemple *via* `dbWriteTable()` avec R). La fonction Oracle [`DBMS_STATS.GATHER_TABLE_STATS()`](https://docs.oracle.com/en/database/oracle/oracle-database/19/arpls/DBMS_STATS.html#GUID-CA6A56B9-0540-45E9-B1D7-D78769B7714C) permet de collecter les statistiques d’une table. Les jointures des tables créées par les utilisateurs avec des tables du SNDS sont ainsi plus rapides.

<div class="note">

Les statistiques sont consultables dans la vue [`USER_TAB_STATISTICS`](https://docs.oracle.com/en/database/oracle/oracle-database/19/refrn/ALL_TAB_STATISTICS.html).

</div>

Pour exécuter `DBMS_STATS.GATHER_TABLE_STATS()` *via* R, on peut utiliser les commandes suivantes :

```language-r
user <- dbGetQuery(con, "SELECT user FROM dual;")
user <- dbQuoteIdentifier(con, user$USER)
dbExecute(
  con,
  "BEGIN DBMS_STATS.GATHER_TABLE_STATS(?, ?); END;",
  params = list(user, "<table>")
)
# [1] 0
```

<div class="note">

`dbQuoteIdentifier()` permet de gérer automatiquement les guillemets, par exemple si le nom d’utilisateur ne commence pas par une lettre. Il est nécessaire d’utiliser un bloc `BEGIN`/`END` car [`EXEC[CUTE]`](https://docs.oracle.com/en/database/oracle/oracle-database/19/sqpug/EXECUTE.html#GUID-A64F29C8-B5C7-426D-82E9-A8E85763F3D6) est une commande propre à SQL\*Plus et n’est pas utilisable *via* ODBC.

</div>

<div class="note">

Ces commandes sont l’équivalent de la macro `%m_stats_table()` fournie par la Cnam pour SAS. Plus de détails sur la macro `%m_stats_table()` sont disponibles dans le guide de bonnes pratiques SAS sur la page d’accueil du portail de la Cnam. Avec SAS, `%include "/opt/app/SAS/sas94config/compute/Lev1/SASApp/SASEnvironment/SASMacro/m_stats_table.sas" / source2;` permet d’afficher le code source de la macro dans le journal.

</div>

### Sensibilité à la casse

En SQL, les mots clés sont insensibles à la casse. Par exemple, `SELECT` et `select` peuvent être utilisés de manière interchangeable, mais on écrit généralement les mots clés en capitale par convention.

Avec Oracle, les identifiants (notamment les noms de tables, de vues et de colonnes) sont insensibles à la casse, sauf s’ils sont entre guillemets doubles. Cela a une implication concrète notamment pour les tables créées *via* `dbWriteTable()` dans R :

```language-r
dbWriteTable(con, "iris", iris)
dbGetQuery(con, "SELECT sepal.length FROM iris WHERE species = 'setosa'")
# Erreur : nanodbc/nanodbc.cpp:1526: 00000: [RStudio][OracleOCI] (3000) Oracle Caller Interface: ORA-00942: Table ou vue inexistante
```

Il est alors nécessaire d’indiquer les identifiants entre guillemets doubles. Puisque les instructions sont elles-mêmes entre guillemets, il est nécessaire d’échapper les guillemets des identifiants avec une des méthodes suivantes :

```language-r
dbGetQuery(
  con,
  "SELECT \"Sepal.Length\" FROM \"iris\" WHERE \"Species\" = 'setosa'"
)
#    Sepal.Length
# 1           5.1
# 2           4.9
# 3           4.7
# …

dbGetQuery(
  con,
  'SELECT "Sepal.Length" FROM "iris" WHERE "Species" = \'setosa\''
)
#    Sepal.Length
# 1           5.1
# 2           4.9
# 3           4.7
# …

dbGetQuery(
  con,
  r"(SELECT "Sepal.Length" FROM "iris" WHERE "Species" = 'setosa')"
)
#    Sepal.Length
# 1           5.1
# 2           4.9
# 3           4.7
# …
```

<div class="note">

[Contrairement à R](https://stat.ethz.ch/R-manual/R-devel/library/base/html/Quotes.html), les guillemets simples (`'`) et les guillemets doubles (`"`) ne sont pas interchangeables en SQL :
- les guillemets simples sont utilisés pour spécifier les valeurs littérales ;
- et les guillemets doubles sont utilisés pour activer la sensibilité à la casse des identifiants.

</div>

### Types `DATE` et `TIMESTAMP`

Les dates du SNDS (par exemple `ER_PRS_F.EXE_SOI_DTD`) sont toutes stockées dans des colonnes de type `DATE` dans Oracle.

<div class="warning">

Contrairement à la norme ANSI, le [type `DATE` de Oracle](https://docs.oracle.com/en/database/oracle/oracle-database/21/nlspg/datetime-data-types-and-time-zone-support.html#GUID-4D95F6B2-8F28-458A-820D-6C05F848CA23) contient les champs `HOUR`, `MINUTE` et `SECOND`. Pour la plupart les dates du SNDS (mais pas toutes, par exemple `IR_BEN_R.BEN_IDT_MAJ`), les champs `HOUR`, `MINUTE` et `SECOND` valent `00:00:00`.

</div>

Le type `TIMESTAMP` est une extension du type `DATE` pouvant stocker les fractions de secondes et un fuseau horaire.

<div class="note">

Aucune date du SNDS n’est stockée dans des colonnes de type `TIMESTAMP`. Les seules colonnes de type `TIMESTAMP` sont dans des tables « système » de Oracle. La liste de ces colonnes est consultable avec `SELECT owner, table_name, column_name, data_type FROM all_tab_cols WHERE data_type LIKE 'TIMESTAMP%';`.

</div>

### Comparaison des valeurs caractères

Il existe [deux sémantiques de comparaison](https://docs.oracle.com/en/database/oracle/oracle-database/19/sqlrf/Data-Type-Comparison-Rules.html#GUID-A114F1F4-A08D-4107-B679-323DC7FEA31C) des valeurs caractères dans Oracle :
- *blank-padded*, c’est à dire que des espaces sont ajoutés à la valeur la plus courte. Cette sémantique est utilisée notamment quand les deux valeurs à comparer sont de type `CHAR` ou littérale.
- *nonpadded*, c’est à dire sans que des espaces ne soient ajoutés à la valeur la plus courte. Cette sémantique est utilisée notamment quand une des deux valeurs à comparer est de type `VARCHAR2`. Il est alors nécessaire d’ajouter des espaces manuellement jusqu’à la longueur requise ou utiliser `LIKE <chaine>%`.

## R et RStudio

La Cnam a un projet de longue date visant à déployer R et RStudio sur le SNDS. Le déploiement en phase de test pour quelques utilisateurs a débuté en 2020. Depuis, le déploiement se poursuit, mais il est encore nécessaire de faire une [demande auprès du support national](mailto:support-national@assurance-maladie.fr?subject=Cr%C3%A9ation%20Habilitation%20RStudio&body=Indiquer%20%3A%0A-%20votre%20identifiant%20de%20connexion%0A-%20votre%20r%C3%A9gion%20et%20votre%20profil%20de%20connexion%0A-%20votre%20adresse%20email) pour avoir accès à R.

<div class="note">

Il est possible d’ouvrir plusieurs [sessions R dans RStudio](https://support.posit.co/hc/en-us/articles/211789298-Multiple-R-Sessions-in-RStudio-Workbench-RStudio-Server-Pro/) afin de lancer différentes analyses en parallèle.

</div>

Contrairement à SAS, peu de documentation est disponible pour utiliser R dans le SNDS. Les principaux documents sont les suivants :
- Cnam. Guide utilisateur RStudio. Disponible sur la [page d’accueil du portail de la Cnam](https://portail.sniiram.ameli.fr/).
- S Allain. [Pratiquer R sur le portail SNDS : Compléments d’un utilisateur au guide Cnam](https://documentation-snds.health-data-hub.fr/files/drees/tuto_r_portail_snds.nb).

Il existe néamoins de la documentation non spécifique au SNDS, telle que :
- La documentation de Posit relatives aux bases de données, avec notamment les parties relatives au [panneau de connexions](https://solutions.posit.co/connections/db/tooling/connections/#connections-pane) de RStudio et à la [pile logicielle](https://solutions.posit.co/connections/db/databases/oracle/) pour connecter R et Oracle ;
- Le blog de Posit, avec notamment un billet relatif à l’[intégration de SQL dans RStudio](https://posit.co/blog/rstudio-1-2-preview-sql/) ;
- Le livre *R Markdown: The Definitive Guide*, avec notamment un chapitre relatif l’[intégration de SQL dans R Markdown](https://bookdown.org/yihui/rmarkdown/language-engines.html#sql).

### Connexion à Oracle avec `{DBI}` et `{odbc}`

[`{DBI}`](https://dbi.r-dbi.org/) permet de connecter R avec différentes bases de données. Il sépare la connectivité à la base de données en un *front-end* et des *back-ends*. `{DBI}` définie une interface (le *front-end*) qui est implémentée par [différent *back-ends*](https://github.com/r-dbi/backends/#readme).

L’interface se compose en particulier de fonctions permettant de se connecter (`dbConnect()`) et se déconnecter (`dbDisconnect()`) de la base de données, d’exécuter des instructions (`dbExecute()`) et des requêtes (`dbGetQuery()`), d’écrire (`dbWriteTable()`) et de lire (`dbReadTable()`) des tables et de lister les tables (`dbListTables()`). Ces fonctions sont stables quelque soit le *back-end* utilisé.

Parmi les *back-ends* disponibles pour se connecter à une base de données Oracle, [`{odbc}`](https://odbc.r-dbi.org/) fait actuellement l’objet du développement le plus actif. `{odbc}` est développé par les auteurs de `{DBI}`. En pratique, la seule utilisation visible du *back-end* est l’appel au pilote (`odbc::odbc()`) dans `dbConnect()`.

Ainsi, la commande pour se connecter à la base de données Oracle est la suivante :

```language-r
library(DBI)
con <- dbConnect(odbc::odbc(), dsn = "IPIAMPR2")
```

Il est nécessaire de se déconnecter à la fin du script :

```language-r
dbDisconnect(con)
```

<div class="warning">

Par défaut, le panneau de connexions affiche tous les objets de la base de données, et pas uniquement ceux de l’utilisateur connecté. Cela a fait l’objet d’une [demande d’amélioration](https://github.com/r-dbi/odbc/issues/605/). Il est possible d’utiliser [`options(connectionObserver = NULL)`](https://github.com/r-dbi/odbc/issues/158/) pour éviter que le panneau de connexion ne liste les objets de la base de données. `dbConnect()` est alors plus rapide.

</div>

### Exécuter des instructions avec `{DBI}`

Une fois connecté à Oracle, il est possible d’exécuter des requêtes ou toutes autres instructions SQL.

Pour exécuter des requêtes, [`dbGetQuery()` est à privilégier](https://dbi.r-dbi.org/reference/dbSendQuery.html#the-data-retrieval-flow). Par exemple, pour récupérer dans un *data frame* dans R les codes CIP correspondant au code ATC de la prégabaline :

```language-r
dbGetQuery(
  con,
  "SELECT pha_cip_c13 FROM ir_pha_r WHERE pha_atc_cla = 'N02BF02';"
)
#       PHA_CIP_C13
# 1   3400930028971
# 2   3400930029015
# 3   3400936512870
# …
```

Pour exécuter des instructions, [`dbExecute()` est à privilégier](https://dbi.r-dbi.org/reference/dbSendStatement.html#the-command-execution-flow). Par exemple, pour créer une table dans Oracle contenant des données démographiques des bénéficiaires :

```language-r
dbExecute(
  con,
  "CREATE TABLE ben AS SELECT ben_idt_ano, ben_nai_ann FROM ir_ben_r;"
)
# [1] 0
```

<div class="note">

Il est possible d’accéder à la table `BEN` ainsi créée *via* la bibliothèque `orauser` dans SAS.

</div>

`{DBI}` permet d’exécuter des [instructions paramétrées](https://dbi.r-dbi.org/articles/DBI-advanced.html#parameterized-queries). Une instruction paramétrée est une instruction dans laquelle des caractères génériques sont utilisés pour indiquer des paramètres dont les valeurs sont fournies au moment de l’exécution. Avec `{odbc}`, le caractère générique à utiliser est `?`. Il suffit d’insérer un ou plusieurs `?` dans l’instruction et d’indiquer les paramètres *via* `params =` dans `dbGetQuery()` ou `dbExecute()`. La requête précédente dans `IR_PHA_R` peut être écrite de la façon suivante :

```language-r
dbGetQuery(
  con,
  "SELECT pha_cip_c13 FROM ir_pha_r WHERE pha_atc_cla = ?;",
  params = "N02BF02   "
)
#       PHA_CIP_C13
# 1   3400930028971
# 2   3400930029015
# 3   3400936512870
# …
```

<div class="note">

Avec la requête paramétrée, le paramètre est passé en `VARCHAR2` et la comparaison est réalisée selon la [sémantique *nonpadded*](#comparaison-des-valeurs-caracteres). La colonne `PHA_ATC_CLA` étant de type `CHAR(10)`, il est nécessaire d’ajouter des espaces manuellement (`"N02BF02␣␣␣"`) ou d’utiliser `PHA_ATC_CLA LIKE ?` avec `params = "N02BF02%"`.

</div>

<div class="note">

Seules des constantes peuvent être passées en paramètre. Pour manipuler les identifiants, il est nécessaire d’utiliser `paste()`, `sprintf()` ou des [alternatives plus sûres](https://solutions.posit.co/connections/db/best-practices/run-queries-safely/).

</div>

<div class="warning">

En raison d’un [*bug* dans le pilote ODBC RStudio](https://github.com/r-dbi/odbc/issues/537/), si on passe un vecteur de longueur > 1 à `params =` dans `dbGetQuery()`, on obtient un *data frame* incomplet contenant uniquement les résultats correspondant au premier élément du vecteur, sans message d’erreur. Ce *bug* concerne uniquement `dbGetQuery()` et pas `dbExecute()`.

</div>

### Mémoire vive

R « travaillant » en mémoire vive (*random access memory*), il est indispensable d’optimiser les scripts compte tenu du volume des données du SNDS. En particulier, il est préférable de faire le maximum de *data management* avec Oracle et d’importer les tables dans R uniquement pour les analyses statistiques.

Les administrateurs de la Cnam ont prévus 3 paliers de mémoire R. Par défaut, les utilisateurs sont au palier minimum. Si un script préalablement optimisé ne fonctionne pas pour cause de mémoire insuffisante (`Error: cannot allocate vector of size n Mb`), il est possible de demander de passer au palier supérieur.

### Dates, heures et fuseaux horaires

Les colonnes [`DATE` de Oracle](#types-date-et-timestamp) sont [restituées en objets de classe `POSIXct`](https://github.com/r-dbi/odbc/issues/644/) dans R (et donc avec un fuseau horaire). L’heure n’est pas affichée lorsqu’elle vaut `00:00:00`. Afin de simplifier la gestion des dates, on peut convertir les objets `POSIXct` où l’heure vaut `00:00:00` en objets `Date` :

```language-r
dte <- dbGetQuery(con, "SELECT DATE '2020-01-01' FROM dual;")
dte[[1]]
# [1] "2020-01-01 UTC"
as.Date(dte[[1]])
# [1] "2020-01-01"
```

<div class="note">

Aucun décalage horaire n’est introduit car, par défaut, `timezone_out = "UTC"` dans [`dbConnect()`](https://odbc.r-dbi.org/reference/dbConnect-OdbcDriver-method.html) et `tz = "UTC"` dans [`as.Date()`](https://stat.ethz.ch/R-manual/R-devel/library/base/html/as.Date.html).

</div>

En cas de besoin spécifique de travailler avec les heures, il faut être vigilant aux fuseaux horaires. Sur le portail de la Cnam, R est par défaut selon le fuseau horaire [`GMT`](https://fr.wikipedia.org/wiki/Temps_moyen_de_Greenwich) :

```language-r
Sys.getenv("TZ")
# [1] "GMT"
Sys.time()
# [1] "2023-12-14 15:28:39 GMT"
```

Pour travailler avec des [objets *date-time*](https://stat.ethz.ch/R-manual/R-devel/library/base/html/DateTimeClasses.html) (`POSIXct` et `POSIXlt`) dans le fuseau horaire français, il faut modifier la variable d’environnement correspondante :

```language-r
Sys.setenv(TZ = "Europe/Paris")
Sys.time()
# [1] "2023-12-14 16:28:39 CET"
```

Pour travailler avec des colonnes de type `TIMESTAMP` dans Oracle, il faut modifier `timezone =` et `timezone_out =` dans `dbConnect()` :

```language-r
query <- "SELECT TIMESTAMP '2020-01-01 12:00:00' FROM dual;"

con <- dbConnect(
  odbc::odbc(),
  dsn = "IPIAMPR2",
  timezone = "Europe/Paris",
  timezone_out = "Europe/Paris"
)
dbGetQuery(con, query)[[1]]
# [1] "2020-01-01 12:00:00 CET"
dbDisconnect(con)
```

Autrement, l’heure n’est pas dans le bon fuseau horaire et/ou il y a un décalage horaire :

```language-r
con <- dbConnect(odbc::odbc(), dsn = "IPIAMPR2")
dbGetQuery(con, query)[[1]]
# [1] "2020-01-01 12:00:00 UTC"
dbDisconnect(con)

con <- dbConnect(odbc::odbc(), dsn = "IPIAMPR2", timezone = "Europe/Paris")
dbGetQuery(con, query)[[1]]
# [1] "2020-01-01 11:00:00 UTC"
dbDisconnect(con)

con <- dbConnect(odbc::odbc(), dsn = "IPIAMPR2", timezone_out = "Europe/Paris")
dbGetQuery(con, query)[[1]]
# [1] "2020-01-01 13:00:00 CET"
dbDisconnect(con)
```

Il faut également être vigilant en cas d’utilisation des [fonctions *date-time*](https://docs.oracle.com/en/database/oracle/oracle-database/19/nlspg/datetime-data-types-and-time-zone-support.html#GUID-429C290A-C201-4272-A369-FF8F4F8E54F7). En particulier, `CURRENT_TIMESTAMP` retourne la date et l’heure dans le fuseau horaire de la session Oracle. Il faut donc paramétrer également ce dernier avec la variable d’environnement [`ORA_SDTZ`](https://docs.oracle.com/en/database/oracle/oracle-database/19/nlspg/datetime-data-types-and-time-zone-support.html#GUID-578B5988-31E2-4D0F-ACEA-95C827F6012B) *avant* `dbConnect()` :

```language-r
query <- "SELECT CURRENT_TIMESTAMP FROM dual;"

Sys.setenv(ORA_SDTZ = "Europe/Paris")
con <- dbConnect(
  odbc::odbc(),
  dsn = "IPIAMPR2",
  timezone = "Europe/Paris",
  timezone_out = "Europe/Paris"
)
dbGetQuery(con, query)[[1]]
# [1] "2023-12-14 17:54:56 CET"
dbDisconnect(con)
```

Autrement, `CURRENT_TIMESTAMP` retourne une heure avec un décalage horaire :

```language-r
Sys.setenv(ORA_SDTZ = "")
con <- dbConnect(
  odbc::odbc(),
  dsn = "IPIAMPR2",
  timezone = "Europe/Paris",
  timezone_out = "Europe/Paris"
)
dbGetQuery(con, query)[[1]]
# [1] "2023-12-14 16:54:56 CET"
dbDisconnect(con)
```

### `{ROracle}` *versus* `{odbc}`

La Cnam recommande d’utiliser [`{ROracle}`](https://cran.r-project.org/package=ROracle) (plutôt que `{odbc}`) comme *back-end* DBI pour se connecter à Oracle. `{ROracle}` est développé par Oracle.

Le [développement](https://cran.r-project.org/web/packages/ROracle/NEWS) de `{ROracle}` est peu actif. De plus, il ne respecte pas la complètement spécification DBI et sa [documentation](https://cran.r-project.org/web/packages/ROracle/ROracle.pdf) est succincte. Cela peut rendre son comportement mystérieux. Son utilisation n’est donc pas davantage présentée dans cette documentation.

À titre illustratif, quelques particularités de `{ROracle}` sont listées ci-dessous :
- Il ne faut pas terminer les instructions par `;`.
- Il faut utiliser `data =` et la notation `:1`, …, `:n` pour passer des paramètres à une instruction (`{ROracle}` n’implémente pas `params =` et `dbBind()` définis dans la [spécification DBI](https://dbi.r-dbi.org/reference/dbBind.html#specification)).
- Il n’est pas possible de passer un vecteur de paramètres à `data =`.
- Les numéros (ou les noms) des paramètres ne sont pas pris en compte.

```language-r
# fonctionne également avec dbname = "IPIAMPR2.WORLD"
con <- dbConnect(ROracle::Oracle(), dbname = "IPIAMPR2")

dbGetQuery(con, "SELECT pha_cip_c13 FROM ir_pha_r;")
# Erreur dans .oci.GetQuery(conn, statement, data = data, prefetch = prefetch,  : 
#   ORA-00933: la commande SQL ne se termine pas correctement

dbGetQuery(
  con,
  "SELECT pha_cip_c13 FROM ir_pha_r WHERE pha_atc_cla = :1",
  data = data.frame("N02BF02   ")
)
#       PHA_CIP_C13
# 1   3400930028971
# 2   3400930029015
# 3   3400936512870
# …

dbGetQuery(
  con,
  "SELECT pha_cip_c13 FROM ir_pha_r WHERE pha_atc_cla = :1",
  data = data.frame(c("N02BF01   ", "N02BF02   "))
)
# Erreur dans .oci.GetQuery(conn, statement, data = data, prefetch = prefetch,  : 
#   bind data has too many rows

dbGetQuery(
  con,
  "SELECT pha_cip_c13 FROM ir_pha_r WHERE pha_atc_cla = :2 AND pha_med_nom LIKE :1",
  data = data.frame("LYRICA%", "N02BF02   ")
)
# [1] PHA_CIP_C13
# <0 lignes> (ou 'row.names' de longueur nulle)

dbGetQuery(
  con,
  "SELECT pha_cip_c13 FROM ir_pha_r WHERE pha_atc_cla = :atc AND pha_med_nom LIKE :med",
  data = data.frame(med = "LYRICA%", atc = "N02BF02   ")
)
# [1] PHA_CIP_C13
# <0 lignes> (ou 'row.names' de longueur nulle)

dbDisconnect(con)
# [1] TRUE
```

## SAS Guide

SAS Guide est le logiciel métier historique de la Cnam. La majorité des ressources disponibles (code et documentation) sont pour SAS. La Cnam propose un guide de bonnes pratiques SAS sur la [page d’accueil du portail de la Cnam](https://portail.sniiram.ameli.fr/).

De plus, certaines données du SNDS sont stockées dans des bibliothèques SAS et ne sont donc pas directement accessibles dans Oracle. Par exemple, les données de la bibliothèque `consopat` sont des vues SAS (fichiers `.sas7bvew`).

La bibliothèque `orauser` permet aux utilisateurs d’échanger des tables entre SAS et Oracle. Il s’agit d’une bibliothèque partagée entre tous les utilisateurs (bien qu’un utilisateur ne puisse pas accéder aux tables d’un autre utilisateur). La Cnam recommande donc de ne pas y stocker inutilement des tables volumineuses.

<div class="note">

Afin de choisir le langage cible pour un nouveau projet, il est important de souligner que SAS n’est actuellement pas disponible sur la plateforme du *Health Data Hub*.

</div>

### Connexion à Oracle avec SAS

[`SAS/ACCESS`](https://documentation.sas.com/doc/en/lrcon/9.4/n06fds2ssxm72jn1l7y97zgvzaud.htm) permet de communiquer avec des bases de données.

[Deux méthodes](https://www.sas.com/content/dam/SAS/en_ca/User%20Group%20Presentations/Montreal-User-Group/Trudel-SQLPassthrough.pdf) sont disponibles :

<table>
  <thead>
    <tr>
      <th></th>
      <th><em>Pass-through</em> implicite</th>
      <th><em>Pass-through</em> explicite</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Connexion</td>
      <td>Dans le <code>libname</code></td>
      <td>Dans un <code>proc sql</code> avec un <code>connect to oracle</code></td>
    </tr>
    <tr>
      <td>Performance</td>
      <td>Moins performant</td>
      <td>Plus performant</td>
    </tr>
    <tr>
      <td>Fonctions utilisables</td>
      <td>Fonctions SAS</td>
      <td>Fonctions Oracle</td>
    </tr>
    <tr>
      <td>Référencement des tables et des vues</td>
      <td><code>libname.table</code> ou <code>libname.vue</code> (par exemple, <code>oravue.ER_PRS_F</code>)</td>
      <td><code>table</code> ou <code>vue</code> (par exemple, <code>ER_PRS_F</code>)</td>
    </tr>
  </tbody>
</table>

La structure générale d’un script en *pass-through* explicite est la suivante :

```language-sas
proc sql;
connect to oracle (path="IPIAMPR2");
select * from connection to oracle (<requête>);
execute (<instruction>) by oracle;
disconnect from oracle;
quit;
```

<div class="note">

La Cnam fournit la macro `%connectora` qui permet de remplacer `connect to oracle (path="IPIAMPR2");`. La macro `%connectora` spécifie également des arguments supplémentaires. Avec SAS, `%include "/opt/app/SAS/sas94config/compute/Lev1/SASApp/SASEnvironment/SASMacro/connectora.sas" / source2;` permet d’afficher le code source de la macro dans le journal.

</div>

### Portabilité du code SQL entre SAS et R

Dans la mesure où les instructions SQL sont exécutées par Oracle quel que soit le logiciel d’origine (SAS ou R), il est possible d’exécuter une même instruction sans modification en *pass-through* explicite dans SAS et *via* `{DBI}` dans R.

Cependant, en raison des [spécificités du *pass-through* implicite](#connexion-a-oracle-avec-sas), il n’est pas possible d’exécuter une même instruction sans modification en *pass-through* implicite et en *pass-through* explicite dans SAS ou *via* `{DBI}` dans R. De plus, il est possible d’utiliser en *pass-through* implicite des constructions qui ne sont pas valide en SQL ANSI et pour Oracle. Par exemple, la requête suivante est valide en *pass-through* implicite mais pas en *pass-through* exlicite dans SAS ou *via* `{DBI}` dans R :

```language-sas
proc sql;
SELECT
  pha_atc_cla,
  pha_atc_lib,
  COUNT(DISTINCT pha_cip_c13) AS n
FROM
  oravue.ir_pha_r
GROUP BY
  pha_atc_cla;
quit;
```

<div class="note">

En *pass-through* exlicite dans SAS ou *via* `{DBI}` dans R, cette requête cause une erreur `ORA-00979` car `PHA_ATC_LIB` apparait dans le `SELECT` sans fonction d’aggrégation alors qu’elle n’apparait pas dans le `GROUP BY`. En *pass-through* implicite, seule la note suivant apparait dans le journal : `NOTE: The query requires remerging summary statistics back with the original data.`

</div>

<div class="note">

De même, en *pass-through* exlicite dans SAS ou *via* `{DBI}` dans R avec Oracle, il n’est pas possible de réécrire une table avec le même nom (erreur `ORA-00955`). Cela est possible en *pass-through* implicite dans les bibliothèques SAS (mais pas dans la bibliothèque `orauser`). Seul l’avertissement suivant apparait dans le journal : `WARNING: This CREATE TABLE statement recursively references the target table. A consequence of this is a possible data integrity problem.`

</div>
