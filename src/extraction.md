# Extraction

## Remboursements de médicaments dans le DCIR

La table [`ER_PRS_F`](https://documentation-snds.health-data-hub.fr/tables/ER_PRS_F/) est la table centrale du DCIR.
Cette table est très volumineuses[^volumineuses].
Cette table est partitionnée selon le mois de flux (colonne `FLX_DIS_DTD`).
Pour des raisons de performance, il est nécessaire de filtrer les requête dans `ER_PRS_F` selon les mois de flux d'intérêt.

> 📝 La Cnam recommande (dans ses formations et sa documentation) de segmenter les requêtes par mois de flux (colonne `FLX_DIS_DTD`).
> D'après des tests sur plusieurs médicaments, plusieurs classes médicamenteuses complètes et des périodes allant de quelques mois à plusieurs années, le gain de performance est minime.
> Après échanges avec les administrateurs SNDS de la Cnam, l'expert Oracle confirme qu'il ne faut pas segmenter les requêtes par mois de flux systématiquement.
> Cela permet de plus de simplifier drastiquement les scripts d'extraction.

Les données relatives aux remboursements de médicaments (notamment code CIP et nombre de boîtes remboursé) sont disponibles dans la table [`ER_PHA_F`](https://documentation-snds.health-data-hub.fr/tables/ER_PHA_F/).

Le référentiel correspondant à la table `ER_PHA_F` est la table `IR_PHA_R`.
Plusieurs colonnes avec des chaines de caractère de longueur variables sont stockées en `CHAR` au lieu de `VARCHAR2`.
Il y a donc des espaces à la fin des chaines de caractères.
Il est nécessaire d'être vigilant à ces espaces en cas filtrage sur ces colonnes, par exemple xxx :

```r
exemple 'ATC' KO vs 'ATC   ' OK  
```

> ⚠️ La table IR_PHA_R contient des erreurs, par exemple :
> - ancien code ATC (PHA_ATC_CLA)
> - nombre d'unité dans la boite ou dasage par unité erroné
> 
> Il est donc indispensable de vérifier manuellement.
> Il est également possible d'utiliser les données de [Thésorimed](https://theso.prod-un.thesorimed.org/) qui sont plus à jour.
> La table IR_PHA_R dérivent des données de Thésorimed

Ressources utiles :
- Documentations collaboratives du SNDS. [Requête type dans la table prestations du DCIR](https://documentation-snds.health-data-hub.fr/snds/fiches/sas_prestation_dcir.html)

[^volumineuses]: À titre d'exemple, le mois de flux de janvier 2022 correspond à 900 millions de lignes dans la table `ER_PRS_F`.

## Extraction `ER_PRS_F` et `ER_PHA_F`

filtrer sur ER_PRS_F.PFS_PRE_NUM <> '00000000' ou ER_PRS_F.BEN_CDI_NIR = '00' allonge excessivement le temps de requête 

Si, pour un cas particulier, il était nécessaire de segmenter une requête dans `ER_PRS_F` par mois de flux, les instructions paramétrées permettent un gain de clarté et de concision significatif par rapport à la méthode proposée par la Cnam avec SAS[^sas] :

```r
create_table <- "
CREATE TABLE
  PRS (
    BEN_NIR_PSA VARCHAR2(17),
    BEN_RNG_GEM NUMBER,
    EXE_SOI_DTD DATE,
    PFS_PRE_NUM VARCHAR2(17)
);
"

dbExecute(conn, create_table)

insert_into <- "
INSERT INTO
  PRS
SELECT
  BEN_NIR_PSA,
  BEN_RNG_GEM,
  EXE_SOI_DTD,
  PFS_PRE_NUM
FROM
  ER_PRS_F
WHERE
  FLX_DIS_DTD = TO_DATE(?, 'YYYY-MM-DD')
FETCH FIRST 3 ROWS ONLY;
"

flx <- seq(as.Date("2020-01-01"), as.Date("2020-03-01"), "month")
dbExecute(conn, insert_into, params = list(flx))
```

> 📝 La clause `FETCH FIRST n ROWS ONLY` permet de limiter le nombre de ligne à extraire.
> Cette clause est utile pour tester une instruction dont le temps d'exécution est long.

> 📝 Pour avoir un ordre de grandeur, les intructions sur ER_PRS_F mettent environ 30 secondes par mois de flux scanné.
> Si une intruction est anormalement longue, il est probable qu'elle ne fasse pas ce qu'il était prévu...
> Il est possible de chronométrer l'exécution des intructions avec `dbExecute(conn, <instruction>) |> system.time()`.


## Validation Open Medic

```
xtr <- "
SELECT
  COUNT(DISTINCT PRS_REG.BEN_IDT_ANO) as COUNT_BEN_IDT_ANO,
  SUM(PRS_REG.PHA_ACT_QSN) AS SUM_PHA_ACT_QSN,
  EXTRACT (YEAR FROM PRS_REG.EXE_SOI_DTD) AS YEAR_EXE_SOI_DTD,
  IR_PHA_R.PHA_ATC_CLA,
  IR_PHA_R.PHA_ATC_LIB
FROM
  PRS_REG
INNER JOIN
  IR_PHA_R
  ON PRS_REG.PHA_PRS_C13 = IR_PHA_R.PHA_CIP_C13
GROUP BY
  EXTRACT (YEAR FROM PRS_REG.EXE_SOI_DTD),
  IR_PHA_R.PHA_ATC_CLA,
  IR_PHA_R.PHA_ATC_LIB
"
xtr <- dbGetQuery(conn, xtr)

om <- list.files(pattern = "NB_20[0-9]{2}_atc5.CSV")
om <- lapply(om, read.csv2)
om <- lapply(om, `[`, c("ATC5", "nbc", "BOITES"))
om <- Map(cbind, om, YEAR = 2015:2020)
om <- do.call(rbind, om)

idx <- sapply(xtr, is.character)
xtr[idx] <- lapply(xtr[idx], trimws)
xtr[xtr$PHA_ATC_CLA == "N02BF01", "PHA_ATC_CLA"] <- "N03AX12"
xtr[xtr$PHA_ATC_CLA == "N02BF02", "PHA_ATC_CLA"] <- "N03AX16"
xtr[xtr$PHA_ATC_CLA == "N02AJ13" & xtr$YEAR_EXE_SOI_DTD != 2020, "PHA_ATC_CLA"] <- "N02AX52"
xtr[xtr$PHA_ATC_LIB == "TRAMADOL ET PARACETAMOL", "PHA_ATC_LIB"] <- "TRAMADOL + PARACETAMOL"
xtr <- aggregate(cbind(COUNT_BEN_IDT_ANO, SUM_PHA_ACT_QSN) ~ PHA_ATC_CLA + PHA_ATC_LIB + YEAR_EXE_SOI_DTD, xtr, sum)

xtr <- merge(xtr, om, by.x = c("PHA_ATC_CLA", "YEAR_EXE_SOI_DTD"), by.y = c("ATC5", "YEAR"), all.x = TRUE)
xtr$nbc_diff = xtr$COUNT_BEN_IDT_ANO / xtr$nbc * 100
xtr$boites_diff = xtr$SUM_PHA_ACT_QSN / xtr$BOITES * 100
```

ressources :
- documentation HDH "requete type DCIR"
- programme SP france
- programme Cnam

[^sas]: Voir la recommandation numéro 5 page 26 du Guide de bonnes pratiques SAS diponible sur la [page d'accueil du portail de la Cnam](https://portail.sniiram.ameli.fr/).

## PMSI

MCO seulement ? -> reco


Gestion des NULLs :
1/ § dans Oracle
https://docs.oracle.com/en/database/oracle/oracle-database/19/sqlrf/Nulls.html#GUID-C785FE74-5F9C-4F70-AC4B-CA5D3010162A
https://documentation-snds.health-data-hub.fr/snds/fiches/valeurs_manquantes.html

```
dbExecute(con, "CREATE TABLE tmp (col1 NUMBER, col2 NUMBER);")
dbExecute(con, "INSERT INTO tmp VALUES (1, 1);")
dbExecute(con, "INSERT INTO tmp VALUES (1, NULL);")
dbExecute(con, "INSERT INTO tmp VALUES (NULL, 1);")
dbExecute(con, "INSERT INTO tmp VALUES (NULL, NULL);")

dbGetQuery(con, "SELECT * from tmp WHERE col1 = col2;")
dbGetQuery(con, "SELECT * from tmp WHERE col1 <> col2;")
dbGetQuery(con, "SELECT * from tmp l INNER JOIN tmp r USING(id);")
dbGetQuery(con, "SELECT * from tmp l LEFT JOIN tmp r USING(id);")
```

2/ note dans la fiche
```
dbGetQuery(con, "select sej_typ, count(*) from t_mco20b group by sej_typ")
dbGetQuery(con, "select count(*) from t_mco20b where sej_typ <> 'B' or sej_typ is null")
dbGetQuery(con, "select count(*) from t_mco20b where sej_typ <> 'B'")
```




références :
- programme SP France

