# Annexes

## Abréviations

- ANSI : *American National Standards Institute*
- Cnam : Caisse Nationale de l’Assurance Maladie
- DBI : *Database Interface*
- ODBC : *Open Database Connectivity*
- SNDS : Système National des Données de Santé
- SQL : *Structured Query Language*

## Bibliographie

- Les supports de formation et la documentation disponibles sur le [portail de la Cnam](https://portail.sniiram.ameli.fr/accueil/#/accueil/tableaurequete/).
- Health Data Hub. [Documentation collaborative du SNDS](https://documentation-snds.health-data-hub.fr/snds/).
- Santé publique France. [Guide d’utilisation du SNDS pour la surveillance et l’observation épidémiologiques](https://www.santepubliquefrance.fr/docs/guide-d-utilisation-du-systeme-national-des-donnees-de-sante/).
- ReDSiam. [Choix de la population de référence dans l’estimation de taux de recours aux soins, d’incidence et de prévalence à partir des données du SNDS](https://documentation-snds.health-data-hub.fr/files/redsiam/202103_Redsiam_GTDenominateurs-ChoixDenominateurs_MLP-2.0.pdf).

## Conventions et notations

Pour R, les paquets, fonctions, et arguments sont notés de la façon suivante : `{paquet}`, `fonction()` et `argument =`.

Les exemples de codes R supposent qu’une [connexion nommée `con`](logiciels.html#connexion-a-oracle-avec-dbi-et-odbc) a préalablement été créée.

Pour SQL, « instruction » désigne une commande valide et « requête » désigne spécifiquement une instruction qui retourne des données.

Dans un extrait de code, `<texte>` indique un texte à remplacer.

<div class="note">

Les encarts « Note » apportent des informations complémentaires facultatives.

</div>

<div class="warning">

Les encarts « Attention » signalent des pièges potentiels ou des *bugs*.

</div>
